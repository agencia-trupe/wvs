<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Mostra;

class MostrasController extends Controller
{
    public function index()
    {
        $mostras = Mostra::ordenados()->get();

        return view('frontend.mostras.index', compact('mostras'));
    }

    public function show(Mostra $mostra)
    {
        return view('frontend.mostras.show', compact('mostra'));
    }
}
