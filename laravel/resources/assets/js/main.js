import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

$('.banners').cycle({
    slides: '>.banner'
});

$('.clipping .thumb').click(function(event) {
    event.preventDefault();

    var $thumb = $(this);

    var url = $thumb.attr('href'),
        titulo = $thumb.find('h3').text(),
        data = $thumb.find('p').text();

    $.get(url, function(imagensHtml) {
        $(
            '.clipping-lightbox h3, .clipping-lightbox p, .clipping-lightbox-imagens'
        ).empty();
        $('.clipping-lightbox').hide();

        $('.clipping-lightbox h3').text(titulo);
        $('.clipping-lightbox p').text(data);
        $('.clipping-lightbox-imagens').html(imagensHtml);

        $('body').css({
            overflow: 'hidden'
        });
        $('.clipping-lightbox').fadeIn();
    });
});

$('.clipping-lightbox-close').click(function(event) {
    event.preventDefault();
    $('body').css({
        overflow: 'visible'
    });
    $('.clipping-lightbox').fadeOut();
});

$(document).on('submit', '#form-contato', function(event) {
    event.preventDefault();

    var $form = $(this),
        $response = $('#form-contato-response');

    if ($form.hasClass('sending')) return false;

    $response.fadeOut('fast');
    $form.addClass('sending');

    $.ajax({
        type: 'POST',
        url: $('base').attr('href') + '/contato',
        data: {
            nome: $('#nome').val(),
            email: $('#email').val(),
            telefone: $('#telefone').val(),
            mensagem: $('#mensagem').val()
        },
        success: function(data) {
            $response
                .fadeOut()
                .text(data.message)
                .fadeIn('slow');
            $form[0].reset();
        },
        error: function(data) {
            if (data.responseJSON) {
                $response
                    .fadeOut()
                    .text('Preencha todos os campos corretamente.')
                    .fadeIn('slow');
            }
        },
        dataType: 'json'
    }).always(function() {
        $form.removeClass('sending');
    });
});
