@extends('frontend.common.template')

@section('content')

    <div class="main portfolio">
        <div class="center full-width-max">
            @foreach($mostras as $mostra)
            <a href="{{ route('mostras.show', $mostra->slug) }}" class="thumb">
                <img src="{{ asset('assets/img/mostras/'.$mostra->capa) }}" alt="">
                <div class="overlay">
                    <h3>{{ $mostra->titulo }}</h3>
                    <p>{{ $mostra->local }}</p>
                </div>
            </a>
            @endforeach
        </div>
    </div>

@endsection
