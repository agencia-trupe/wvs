<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Projeto;
use App\Models\ProjetoCategoria;

class PortfolioController extends Controller
{
    public function index(ProjetoCategoria $categoria)
    {
        if (!$categoria->exists) {
            $categoria = ProjetoCategoria::ordenados()->firstOrFail();
        }

        $projetos = $categoria->portfolio;

        view()->share('portfolioCategoriaSelecionada', $categoria->slug);

        return view('frontend.portfolio.index', compact('projetos'));
    }

    public function show(ProjetoCategoria $categoria, Projeto $projeto)
    {
        view()->share('portfolioCategoriaSelecionada', $categoria->slug);

        return view('frontend.portfolio.show', compact('projeto'));
    }
}
