<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use Carbon\Carbon;

class Clipping extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'clipping';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('data', 'DESC')->orderBy('id', 'DESC');
    }

    public function setDataAttribute($date)
    {
        $this->attributes['data'] = Carbon::createFromFormat('m/Y', $date)->format('Y-m');
    }

    public function getDataAttribute($date)
    {
        return Carbon::createFromFormat('Y-m', $date)->format('m/Y');
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\ClippingImagem', 'clipping_id')->ordenados();
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            'width'  => 270,
            'height' => 360,
            'path'   => 'assets/img/clipping/'
        ]);
    }
}
