<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('perfil', 'PerfilController@index')->name('perfil');
    Route::get('portfolio/{portfolio_categoria?}', 'PortfolioController@index')->name('portfolio');
    Route::get('portfolio/{portfolio_categoria}/{projeto_slug}', 'PortfolioController@show')->name('portfolio.show');
    Route::get('mostras', 'MostrasController@index')->name('mostras');
    Route::get('mostras/{mostra_slug}', 'MostrasController@show')->name('mostras.show');
    Route::get('clipping', 'ClippingController@index')->name('clipping');
    Route::get('clipping/{clipping}', 'ClippingController@show')->name('clipping.show');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function () {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
        Route::resource('clipping', 'ClippingController');
        Route::get('clipping/{clipping}/imagens/clear', [
            'as'   => 'painel.clipping.imagens.clear',
            'uses' => 'ClippingImagensController@clear'
        ]);
        Route::resource('clipping.imagens', 'ClippingImagensController', ['parameters' => ['imagens' => 'imagens_clipping']]);
        Route::resource('mostras', 'MostrasController');
        Route::get('mostras/{mostras}/imagens/clear', [
            'as'   => 'painel.mostras.imagens.clear',
            'uses' => 'MostrasImagensController@clear'
        ]);
        Route::resource('mostras.imagens', 'MostrasImagensController', ['parameters' => ['imagens' => 'imagens_mostras']]);
        Route::resource('portfolio/categorias', 'PortfolioCategoriasController', ['parameters' => ['categorias' => 'categorias_portfolio']]);
        Route::resource('portfolio', 'PortfolioController');
        Route::get('portfolio/{portfolio}/imagens/clear', [
            'as'   => 'painel.portfolio.imagens.clear',
            'uses' => 'PortfolioImagensController@clear'
        ]);
        Route::resource('portfolio.imagens', 'PortfolioImagensController', ['parameters' => ['imagens' => 'imagens_portfolio']]);
        Route::resource('perfil', 'PerfilController', ['only' => ['index', 'update']]);
        Route::resource('banners', 'BannersController');
        Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);

        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function () {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
