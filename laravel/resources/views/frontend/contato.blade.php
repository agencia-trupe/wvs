@extends('frontend.common.template')

@section('content')

    <div class="main contato">
        <div class="center">
            <form action="" id="form-contato" method="POST">
                <input type="text" name="nome" id="nome" placeholder="nome" required>
                <input type="email" name="email" id="email" placeholder="e-mail" required>
                <input type="text" name="telefone" id="telefone" placeholder="telefone">
                <textarea name="mensagem" id="mensagem" placeholder="mensagem" required></textarea>
                <input type="submit" value="ENVIAR">
                <div id="form-contato-response">Mensagem enviada com susex</div>
            </form>

            <div class="informacoes">
                <h2>{{ $contato->telefone }}</h2>
                <p>{{ $contato->endereco }}</p>
            </div>
        </div>

        <div class="mapa">
            {!! $contato->google_maps !!}
        </div>
    </div>

@endsection
