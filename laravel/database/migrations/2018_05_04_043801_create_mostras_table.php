<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMostrasTable extends Migration
{
    public function up()
    {
        Schema::create('mostras', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('titulo');
            $table->string('local');
            $table->string('capa');
            $table->timestamps();
        });

        Schema::create('mostras_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mostra_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
            $table->foreign('mostra_id')->references('id')->on('mostras')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('mostras_imagens');
        Schema::drop('mostras');
    }
}
