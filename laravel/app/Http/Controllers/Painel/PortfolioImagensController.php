<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PortfolioImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Projeto;
use App\Models\ProjetoImagem;

use App\Helpers\CropImage;

class PortfolioImagensController extends Controller
{
    public function index(Projeto $registro)
    {
        $imagens = ProjetoImagem::projeto($registro->id)->ordenados()->get();

        return view('painel.portfolio.imagens.index', compact('imagens', 'registro'));
    }

    public function show(Projeto $registro, ProjetoImagem $imagem)
    {
        return $imagem;
    }

    public function store(Projeto $registro, PortfolioImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = ProjetoImagem::uploadImagem();
            $input['projeto_id'] = $registro->id;

            $imagem = ProjetoImagem::create($input);

            $view = view('painel.portfolio.imagens.imagem', compact('registro', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Projeto $registro, ProjetoImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.portfolio.imagens.index', $registro)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(Projeto $registro)
    {
        try {

            $registro->imagens()->delete();
            return redirect()->route('painel.portfolio.imagens.index', $registro)
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
