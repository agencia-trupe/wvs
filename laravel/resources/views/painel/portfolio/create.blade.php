@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Portfolio /</small> Adicionar Projeto</h2>
    </legend>

    {!! Form::open(['route' => 'painel.portfolio.store', 'files' => true]) !!}

        @include('painel.portfolio.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
