@extends('frontend.common.template')

@section('content')

    <div class="main portfolio">
        <div class="portfolio-categorias-mobile">
            <div class="center">
                @foreach($portfolioCategorias as $slug => $categoria)
                    <a href="{{ route('portfolio', $slug) }}" @if($portfolioCategoriaSelecionada === $slug) class="active" @endif>{{ $categoria }}</a>
                @endforeach
            </div>
        </div>
        <div class="center full-width-max">
            @foreach($projetos as $projeto)
            <a href="{{ route('portfolio.show', [$projeto->categoria->slug, $projeto->slug]) }}" class="thumb">
                <img src="{{ asset('assets/img/portfolio/'.$projeto->capa) }}" alt="">
                <div class="overlay">
                    <h3>{{ $projeto->titulo }}</h3>
                    <p>{{ $projeto->local }}</p>
                </div>
            </a>
            @endforeach
        </div>
    </div>

@endsection
