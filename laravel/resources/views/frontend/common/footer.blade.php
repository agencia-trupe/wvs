    <footer>
        <div class="center">
            <p>
                © {{ date('Y') }} {{ $config->nome_do_site }} - Todos os direitos reservados.
                <span>|</span>
                <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:
                <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
            </p>
        </div>
    </footer>
