<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index, follow">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <base href="{{ url('/') }}">

    <meta name="author" content="Trupe Agência Criativa">
    <meta name="copyright" content="{{ date('Y') }} Trupe Agência Criativa">
    <meta name="description" content="{{ $config->description }}">
    <meta name="keywords" content="{{ $config->keywords }}">

    <meta property="og:title" content="{{ $config->title }}">
    <meta property="og:description" content="{{ $config->description }}">
    <meta property="og:site_name" content="{{ $config->title }}">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ Request::url() }}">
@if($config->imagem_de_compartilhamento)
    <meta property="og:image" content="{{ asset('assets/img/'.$config->imagem_de_compartilhamento) }}">
@endif

    <title>{{ $config->title }}</title>

    {!! Tools::loadCss('css/vendor.main.css') !!}
    {!! Tools::loadCss('vendor/slick-carousel/slick/slick.css') !!}
    {!! Tools::loadCss('vendor/slick-carousel/slick/slick-theme.css') !!}
    {!! Tools::loadCss('css/main.css') !!}
</head>
<body>
    @include('frontend.common.header')
    @yield('content')
@unless(Route::currentRouteName() == 'home')
    @include('frontend.common.footer')
@endunless

    {!! Tools::loadJquery() !!}
    {!! Tools::loadJs('js/vendor.main.js') !!}
    {!! Tools::loadJs('vendor/slick-carousel/slick/slick.min.js') !!}
    {!! Tools::loadJs('js/main.js') !!}

@if($config->analytics)
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', '{{ $config->analytics }}', 'auto');
        ga('send', 'pageview');
    </script>
@endif
</body>
</html>
