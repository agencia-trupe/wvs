    <header @if(Route::currentRouteName() == 'home') class="home" @endif>
        <nav id="nav-mobile">
            @include('frontend.common.nav')
        </nav>

        <div class="center">
            <a href="{{ route('home') }}" class="logo">{{ $config->nome_do_site }}</a>

            <nav id="nav-desktop">
                @include('frontend.common.nav')
            </nav>

            <div class="button-wrapper">
                <button id="mobile-toggle" type="button" role="button">
                    <span class="lines"></span>
                </button>
            </div>
        </div>
    </header>
