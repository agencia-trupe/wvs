<?php

use Illuminate\Database\Seeder;

class ConfiguracoesSeeder extends Seeder
{
    public function run()
    {
        DB::table('configuracoes')->insert([
            'nome_do_site'               => 'WVS',
            'title'                      => 'WVS Arquitetura & Interiores',
            'description'                => '',
            'keywords'                   => '',
            'imagem_de_compartilhamento' => '',
            'analytics'                  => '',
        ]);
    }
}
