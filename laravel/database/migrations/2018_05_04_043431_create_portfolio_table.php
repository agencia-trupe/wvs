<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfolioTable extends Migration
{
    public function up()
    {
        Schema::create('portfolio_categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('portfolio', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('portfolio_categoria_id')->unsigned()->nullable();
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('titulo');
            $table->string('local');
            $table->string('capa');
            $table->foreign('portfolio_categoria_id')->references('id')->on('portfolio_categorias')->onDelete('set null');
            $table->timestamps();
        });

        Schema::create('portfolio_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('projeto_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
            $table->foreign('projeto_id')->references('id')->on('portfolio')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('portfolio_imagens');
        Schema::drop('portfolio');
        Schema::drop('portfolio_categorias');
    }
}
