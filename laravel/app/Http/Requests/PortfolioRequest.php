<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PortfolioRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'portfolio_categoria_id' => 'required',
            'titulo' => 'required',
            'local' => 'required',
            'capa' => 'required|image',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
        }

        return $rules;
    }
}
