@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Mostras /</small> Adicionar Mostra</h2>
    </legend>

    {!! Form::open(['route' => 'painel.mostras.store', 'files' => true]) !!}

        @include('painel.mostras.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
