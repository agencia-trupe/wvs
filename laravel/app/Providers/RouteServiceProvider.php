<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
		$router->model('clipping', 'App\Models\Clipping');
		$router->model('imagens_clipping', 'App\Models\ClippingImagem');
		$router->model('mostras', 'App\Models\Mostra');
		$router->model('imagens_mostras', 'App\Models\MostraImagem');
		$router->model('portfolio', 'App\Models\Projeto');
		$router->model('categorias_portfolio', 'App\Models\ProjetoCategoria');
		$router->model('imagens_portfolio', 'App\Models\ProjetoImagem');
		$router->model('perfil', 'App\Models\Perfil');
		$router->model('banners', 'App\Models\Banner');
		$router->model('configuracoes', 'App\Models\Configuracoes');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('portfolio_categoria', function($value) {
            return \App\Models\ProjetoCategoria::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('projeto_slug', function($value) {
            return \App\Models\Projeto::whereSlug($value)->firstOrFail();
        });
        $router->bind('mostra_slug', function($value) {
            return \App\Models\Mostra::whereSlug($value)->firstOrFail();
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
