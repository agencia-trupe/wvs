<div class="link">
    <a href="{{ route('home') }}" @if(Tools::isActive('home')) class="active" @endif>home</a>
</div>
<div class="link">
    <a href="{{ route('perfil') }}" @if(Tools::isActive('perfil')) class="active" @endif>perfil</a>
</div>
<div class="link">
    <a href="{{ route('portfolio') }}" @if(Tools::isActive('portfolio*')) class="active" @endif>portfólio</a>
    @if(Tools::isActive('portfolio*'))
        <div class="categorias">
            @foreach($portfolioCategorias as $slug => $categoria)
            <a href="{{ route('portfolio', $slug) }}" @if($portfolioCategoriaSelecionada === $slug) class="active" @endif>{{ $categoria }}</a>
            @endforeach
        </div>
    @endif
</div>
<div class="link">
    <a href="{{ route('mostras') }}" @if(Tools::isActive('mostras*')) class="active" @endif>mostras</a>
</div>
<div class="link">
    <a href="{{ route('clipping') }}" @if(Tools::isActive('clipping')) class="active" @endif>clipping</a>
</div>
<div class="link">
    <a href="{{ route('contato') }}" @if(Tools::isActive('contato')) class="active" @endif>contato</a>
</div>
<div class="social">
    @if($contato->instagram)
        <a href="{{ $contato->instagram }}" target="_blank" class="instagram">instagram</a>
    @endif
    @if($contato->facebook)
        <a href="{{ $contato->facebook }}" target="_blank" class="facebook">facebook</a>
    @endif
    @if($contato->pinterest)
        <a href="{{ $contato->pinterest }}" target="_blank" class="pinterest">pinterest</a>
    @endif
</div>
