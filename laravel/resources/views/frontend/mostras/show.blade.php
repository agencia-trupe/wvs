@extends('frontend.common.template')

@section('content')

    <div class="main portfolio">
        <div class="center">
            <div class="portfolio-show">
                <h1>{{ $mostra->titulo }}</h1>
                <h2>{{ $mostra->local }}</h2>

                <div class="imagens">
                @foreach($mostra->imagens as $imagem)
                    <img src="{{ asset('assets/img/mostras/imagens/'.$imagem->imagem) }}" alt="" data-pin-description="{{ $mostra->titulo }} ({{ $mostra->local }}) &middot; {{ $config->title }}">
                @endforeach
                </div>

                <a href="{{ route('mostras') }}" class="voltar">VOLTAR</a>
            </div>
        </div>
    </div>

    <script async defer data-pin-hover="true" data-pin-lang="pt-br" data-pin-tall="true" src="//assets.pinterest.com/js/pinit.js"></script>

@endsection



