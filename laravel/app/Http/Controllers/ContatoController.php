<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Contato;
use App\Http\Requests\ContatosRecebidosRequest;
use App\Models\ContatoRecebido;

class ContatoController extends Controller
{
    public function index()
    {
        $contato = Contato::first();

        return view('frontend.contato', compact('contato'));
    }

    public function post(ContatosRecebidosRequest $request, ContatoRecebido $contatoRecebido)
    {
        $input = $request->all();

        $contatoRecebido->create($input);

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.contato', $input, function ($message) use ($request, $contato) {
                $message->to($contato->email, 'WVS')
                        ->subject('[CONTATO] WVS')
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        $response = [
            'status'  => 'success',
            'message' => 'Mensagem enviada com sucesso!'
        ];

        return response()->json($response);
    }
}
