@extends('frontend.common.template')

@section('content')

    <div class="main clipping">
        <div class="center full-width">
            @foreach($clippings as $clipping)
            <a href="{{ route('clipping.show', $clipping->id) }}" class="thumb">
                <img src="{{ asset('assets/img/clipping/'.$clipping->capa) }}" alt="">
                <div class="overlay">
                    <h3>{{ $clipping->titulo }}</h3>
                    <p>{{ Tools::formataData($clipping->data) }}</p>
                </div>
            </a>
            @endforeach
        </div>
    </div>

    <div class="clipping-lightbox">
        <div class="center">
            <h3></h3>
            <p></p>

            <a href="#" class="clipping-lightbox-close"></a>

            <div class="clipping-lightbox-imagens">
            </div>
        </div>
    </div>

@endsection
