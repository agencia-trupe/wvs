<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\MostrasImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Mostra;
use App\Models\MostraImagem;

use App\Helpers\CropImage;

class MostrasImagensController extends Controller
{
    public function index(Mostra $registro)
    {
        $imagens = MostraImagem::mostra($registro->id)->ordenados()->get();

        return view('painel.mostras.imagens.index', compact('imagens', 'registro'));
    }

    public function show(Mostra $registro, MostraImagem $imagem)
    {
        return $imagem;
    }

    public function store(Mostra $registro, MostrasImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = MostraImagem::uploadImagem();
            $input['mostra_id'] = $registro->id;

            $imagem = MostraImagem::create($input);

            $view = view('painel.mostras.imagens.imagem', compact('registro', 'imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Mostra $registro, MostraImagem $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.mostras.imagens.index', $registro)
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function clear(Mostra $registro)
    {
        try {

            $registro->imagens()->delete();
            return redirect()->route('painel.mostras.imagens.index', $registro)
                             ->with('success', 'Imagens excluídas com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagens: '.$e->getMessage()]);

        }
    }
}
