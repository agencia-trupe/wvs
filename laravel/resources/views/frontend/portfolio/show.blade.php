@extends('frontend.common.template')

@section('content')

    <div class="main portfolio">
        <div class="portfolio-categorias-mobile">
            <div class="center">
                @foreach($portfolioCategorias as $slug => $categoria)
                    <a href="{{ route('portfolio', $slug) }}" @if($portfolioCategoriaSelecionada === $slug) class="active" @endif>{{ $categoria }}</a>
                @endforeach
            </div>
        </div>
        <div class="center">
            <div class="portfolio-show">
                <h1>{{ $projeto->titulo }}</h1>
                <h2>{{ $projeto->local }}</h2>

                <div class="imagens">
                @foreach($projeto->imagens as $imagem)
                    <img src="{{ asset('assets/img/portfolio/imagens/'.$imagem->imagem) }}" alt="" data-pin-description="{{ $projeto->titulo }} ({{ $projeto->local }}) &middot; {{ $config->title }}">
                @endforeach
                </div>

                <a href="{{ route('portfolio') }}" class="voltar">VOLTAR</a>
            </div>
        </div>
    </div>

    <script async defer data-pin-hover="true" data-pin-lang="pt-br" data-pin-tall="true" src="//assets.pinterest.com/js/pinit.js"></script>

@endsection


