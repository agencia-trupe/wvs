<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\MostrasRequest;
use App\Http\Controllers\Controller;

use App\Models\Mostra;

class MostrasController extends Controller
{
    public function index()
    {
        $registros = Mostra::ordenados()->get();

        return view('painel.mostras.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.mostras.create');
    }

    public function store(MostrasRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Mostra::upload_capa();

            Mostra::create($input);

            return redirect()->route('painel.mostras.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Mostra $registro)
    {
        return view('painel.mostras.edit', compact('registro'));
    }

    public function update(MostrasRequest $request, Mostra $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['capa'])) $input['capa'] = Mostra::upload_capa();

            $registro->update($input);

            return redirect()->route('painel.mostras.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Mostra $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.mostras.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
