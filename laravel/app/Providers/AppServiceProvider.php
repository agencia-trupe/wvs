<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function($view) {
            $view->with('config', \App\Models\Configuracoes::first());
        });
        view()->composer('painel.common.nav', function($view) {
            $view->with('contatosNaoLidos', \App\Models\ContatoRecebido::naoLidos()->count());
        });
        view()->composer('frontend.common.nav', function($view) {
            $view->with('contato', \App\Models\Contato::first());
        });
        view()->composer(['frontend.common.nav', 'frontend.portfolio.*'], function($view) {
            $view->with('portfolioCategorias', \App\Models\ProjetoCategoria::ordenados()->lists('titulo', 'slug'));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
