@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Portfolio /</small> Editar Categoria</h2>
    </legend>

    {!! Form::model($categoria, [
        'route'  => ['painel.portfolio.categorias.update', $categoria->id],
        'method' => 'patch'])
    !!}

    @include('painel.portfolio.categorias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
