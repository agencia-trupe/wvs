<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class MostraImagem extends Model
{
    protected $table = 'mostras_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeMostra($query, $id)
    {
        return $query->where('mostra_id', $id);
    }

    public static function uploadImagem()
    {
        return CropImage::make('imagem', [
            [
                'width'   => 180,
                'height'  => 180,
                'path'    => 'assets/img/mostras/imagens/thumbs/'
            ],
            [
                'width'   => null,
                'height'  => 650,
                'path'    => 'assets/img/mostras/imagens/'
            ]
        ]);
    }
}
