@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Mostras /</small> Editar Mostra</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.mostras.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.mostras.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
